//
//  Message.swift
//  ChatDemo
//
//  Created by flydino on 12/21/20.
//

import UIKit

class Message: NSObject {
    var id: String = ""
    var content: String?
    var createBy: String = ""
    var dateCreate: Int = 0
    var imageURL: String?
    var type: Int {
        return content == nil ? 1 : 0
    }
    
    init(content: String? = nil, createBy: String, imageURL: String? = nil) {
        super.init()
        self.content = content
        self.createBy = createBy
        self.dateCreate = Int(Date().timeIntervalSince1970)
        self.imageURL = imageURL
        self.id = "\(dateCreate)"
    }
    
    init(dictionary: [String: Any]) {
        if let content = dictionary["content"] as? String {
            self.content = content
        }
        if let createBy = dictionary["createBy"] as? String {
            self.createBy = createBy
        }
        if let imageURL = dictionary["imageURL"] as? String {
            self.imageURL = imageURL
        }
        if let dateCreate = dictionary["dateCreate"] as? Int {
            self.dateCreate = dateCreate
            self.id = "\(dateCreate)"
        }
    }
    
    func toDictionary() -> [String: Any] {
        var dict = [String: Any]()
        dict["content"] = content
        dict["createBy"] = createBy
        dict["dateCreate"] = dateCreate
        dict["imageURL"] = imageURL
        return dict
    }
}
