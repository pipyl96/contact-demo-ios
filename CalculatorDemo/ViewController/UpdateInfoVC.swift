//
//  UpdateInfoVC.swift
//  CalculatorDemo
//
//  Created by flydino on 12/22/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit
import MBProgressHUD

enum InfoType: String {
    case fullName = "Họ tên"
    case phoneNumber = "Số điện thoại"
    case address = "Địa chỉ"
    case mail = "Mail"
}

class UpdateInfoVC: UIViewController {

    @IBOutlet weak var textField: UITextField!
    
    var type: InfoType!
    var info: String? = ""
    var didUpdateInfo: ((String) ->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = type.rawValue
        self.textField.text = info
        switch type {
        case .phoneNumber:
            textField.keyboardType = .numberPad
        default:
            textField.keyboardType = .default
        }
        textField.becomeFirstResponder()
    }
    
    @IBAction func didClickUpdate(_ sender: Any) {
        guard let text = textField.text else {
            return
        }
        let dict: [String: Any]
        
        switch type {
        case .fullName:
            dict = ["name": text]
        case .phoneNumber:
            dict = ["phone": text]
        case .address:
            dict = ["address": text]
        default:
            dict = ["email": text]
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        FBStorageManager.shared.updateInfo(dict) { [weak self] (error) in
            guard let self = self else { return }
            MBProgressHUD.hide(for: self.view, animated: true)
            if let error = error {
                print("Update info failed! \(error.localizedDescription)")
                return
            }
            self.didUpdateInfo?(text)
            self.navigationController?.popViewController(animated: true)
        }
    }
}
