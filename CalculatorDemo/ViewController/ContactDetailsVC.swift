//
//  ContactDetailsVC.swift
//  CalculatorDemo
//
//  Created by PiPyL on 10/6/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit
import SDWebImage
import Firebase

class ContactDetailsVC: UIViewController { //hien thi 3 lan

    @IBOutlet weak var mailView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var avatarView: UIView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var mailLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var jobLabel: UILabel!
    
    @IBOutlet weak var avatarImageView: UIImageView!
    
    var contactModel: ContactModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        self.navigationItem.title = contactModel?.name

        nameLabel.text = contactModel?.name
        phoneLabel.text = contactModel?.phone
        mailLabel.text = contactModel?.mail
        jobLabel.text = contactModel?.job
        addressLabel.text = contactModel?.address
        
        contentView.layer.shadowColor = UIColor.gray.cgColor
        contentView.layer.shadowRadius = 7
        contentView.layer.shadowOpacity = 1
        contentView.layer.shadowOffset = CGSize(width: 0, height: 0)
        contentView.layer.cornerRadius = 12
                
        avatarView.layer.cornerRadius = avatarView.frame.width / 2
        avatarView.layer.shadowColor = UIColor.gray.cgColor
        avatarView.layer.shadowRadius = 8
        avatarView.layer.shadowOpacity = 1
        avatarView.layer.shadowOffset = CGSize(width: 0, height: 0)
        avatarImageView.layer.cornerRadius = avatarImageView.frame.width / 2
        avatarImageView.isUserInteractionEnabled = contactModel?.id == Auth.auth().currentUser?.uid
        
        let tapAvatar = UITapGestureRecognizer(target: self, action: #selector(didTapAvatar))
        avatarImageView.addGestureRecognizer(tapAvatar)
        avatarImageView.sd_setImage(with: URL(string: contactModel?.avatar ?? ""), completed: nil)
        
        if contactModel?.id != Auth.auth().currentUser?.uid {
            let chatBarButton = UIBarButtonItem(title: "Chat", style: .done, target: self, action: #selector(didClickChat))
            self.navigationItem.rightBarButtonItem = chatBarButton
        }
    }
    
    @objc func didTapAvatar() {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                self.openCamera()
            }))

            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                self.openGallary()
            }))

            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func openCamera() {
        let imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }

    private func openGallary() {
        let imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @objc func didClickChat() {
        if contactModel?.id == Auth.auth().currentUser?.uid {
            let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "AddContactVC") as! AddContactVC
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
            vc.nameCustomer = contactModel?.name ?? "Chat"
            vc.customerID = contactModel?.id
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    private func showUpdateInfoVC(type: InfoType) {
        if contactModel?.id != Auth.auth().currentUser?.uid {
            return
        }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "UpdateInfoVC") as! UpdateInfoVC
        vc.type = type
        switch type {
        case .fullName:
            vc.info = nameLabel.text
        case .phoneNumber:
            vc.info = phoneLabel.text
        case .address:
            vc.info = addressLabel.text
        default:
            break
        }
        vc.didUpdateInfo = { [weak self] (info) in
            guard let self = self else {
                return
            }
            switch type {
            case .fullName:
                self.nameLabel.text = info
            case .phoneNumber:
                self.phoneLabel.text = info
            case .address:
                self.addressLabel.text = info
            default:
                self.mailLabel.text = info
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - Action
    
    @IBAction func didClickFullName(_ sender: Any) {
        showUpdateInfoVC(type: .fullName)
    }
    
    @IBAction func didClickPhoneNumber(_ sender: Any) {
        showUpdateInfoVC(type: .phoneNumber)
    }
    
    @IBAction func didClickAddress(_ sender: Any) {
        showUpdateInfoVC(type: .address)
    }
    
    @IBAction func didClickMail(_ sender: Any) {
        showUpdateInfoVC(type: .mail)
    }
    
    @IBAction func didClickJob(_ sender: Any) {
    }
}

//MARK: - UIImagePickerControllerDelegate
extension ContactDetailsVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let imageURL = info[UIImagePickerController.InfoKey.imageURL] as? URL else {
            return
        }
                
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            FBStorageManager.shared.updateImage(image: pickedImage, name: imageURL.lastPathComponent) { [weak self] (url) in
                guard let self = self else { return }
                if let url = url {
                    FBStorageManager.shared.updateInfo(["avatar": url.absoluteString]) { (error) in
                        
                    }
                }
            }
            avatarImageView.image = pickedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
}
