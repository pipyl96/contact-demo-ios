//
//  SignInViewController.swift
//  CalculatorDemo
//
//  Created by PiPyL on 9/22/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit
import FirebaseAuth
import Photos
import MBProgressHUD

class SignInViewController: UIViewController {
    
    //Bien
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var parentUserNameView: UIView!
    @IBOutlet weak var userNameView: UIView!
    @IBOutlet weak var parentPasswordView: UIView!
    @IBOutlet weak var passwordView: UIView!
    
    @IBOutlet weak var signInButton: UIButton! //Khai bao bien
    
    @IBOutlet weak var wellComeBackLabel: UILabel!
    @IBOutlet weak var signInLabel: UILabel!
    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var name: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func setupUI() { //thiet lap giao dien
        avatarImageView.layer.cornerRadius = 70
        parentUserNameView.layer.cornerRadius = 15
        userNameView.layer.cornerRadius = 15
        parentPasswordView.layer.cornerRadius = 15
        passwordView.layer.cornerRadius = 15
        
        self.navigationItem.title = "Sign In Screen"
        
        let tapAvatar = UITapGestureRecognizer(target: self, action: #selector(didClickAvatar))
        avatarImageView.addGestureRecognizer(tapAvatar)
    }
    
    @objc func didClickAvatar() {
    }
    
    func validateSignIn() { //kiem tra du lieu dang nhap
        if let userName = userNameTextField.text,
            let password = passwordTextField.text,
            userName != "",
            password != "",
            password.count >= 6 {
                print("Validate success: " + userName + " || " + password)
            } else {
                print("Validate fail")
        }
    }
    
    private func showHomeVC() {
        let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let contactViewController = mainStoryboard.instantiateViewController(withIdentifier: "ContactViewController")
        self.navigationController?.pushViewController(contactViewController, animated: true)
    }
    
    //Function
    @IBAction func didClickSignInButton(_ sender: Any) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Auth.auth().signIn(withEmail: userNameTextField.text!, password: passwordTextField.text!) { (result, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let error = error {
                print("ERROR LOGIN: \(error.localizedDescription)")
            } else {
                self.showHomeVC()
            }
        }
    }
    
    @IBAction func didClickSignUpButton(_ sender: Any) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Auth.auth().createUser(withEmail: userNameTextField.text!, password: passwordTextField.text!) { (result, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let result = result {
                FBStorageManager.shared.registerAccount(id: result.user.uid, mail: self.userNameTextField.text!) { (error) in
                    if let error = error {
                        print(error)
                    } else {
                        self.showHomeVC()
                    }
                }
            }
        }
    }
}
