//
//  AddContactVC.swift
//  CalculatorDemo
//
//  Created by PiPyL on 10/16/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit

//Buoc 1
protocol AddContactVCDelegate: class {
    func didFinishAddContact(contactModel: ContactModel)
}

class AddContactVC: UIViewController {

    @IBOutlet weak var fullNameTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var mailTF: UITextField!
    
    //Buoc 1: Tao closure
    var didClickAddContact: ((_ contact: ContactModel) -> Void)?
    
    //Buoc 2
    weak var delegate: AddContactVCDelegate?
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Cập nhật"
    }
    
    @objc func didClickAddBarButton() {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "UpdateAddressVC") {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func didClickAddButton(_ sender: Any) {
        if let fullName = fullNameTF.text, let phone = phoneTF.text, let mail = mailTF.text {
            let avatar = "ic_avatar4"
            let contactModel = ContactModel()
            contactModel.firstName = fullName
            contactModel.lastName = fullName
            contactModel.phone = phone
            contactModel.mail = mail
            contactModel.avatar = avatar
            
            addContact(with: contactModel)
        }
    }
    
    //MARK: - APIs
    func addContact(with contact: ContactModel) {
        APIController.shared.addContact(with: contact) { [weak self] (contact, error) in
            guard let self = self else { return }
            if let error = error {
                print(error.localizedDescription)
            }
            if let contact = contact {
                DispatchQueue.main.async {
                    self.didClickAddContact?(contact)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
}
