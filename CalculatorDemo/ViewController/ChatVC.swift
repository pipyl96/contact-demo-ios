//
//  ChatVC.swift
//  CalculatorDemo
//
//  Created by flydino on 12/21/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit
import MBProgressHUD
import Firebase

class ChatVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    private var messages = [Message]()
    private var conversationID: String?
    private let placeholder = "Nhập tin nhắn..."
    private var isFirst = true
    
    var nameCustomer: String = "Chat"
    var customerID: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        checkAndCreateConversation()
        
        navigationItem.title = nameCustomer
        textView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        tableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize" {
            if let newvalue = change?[.newKey] {
                let newsize  = newvalue as! CGSize
                heightConstraint.constant = newsize.height < 45 ? 45 : newsize.height
            }
        }
    }
    
    //MARK: - Private
    private func observeMessages() {
        guard let id = conversationID else {
            return
        }
        MBProgressHUD.showAdded(to: self.view, animated: true)
        FBStorageManager.shared.getMessages(conversationID: id) { (messages, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let messages = messages {
                self.messages = messages
                self.sortMessages()
            }
        }
    }
    
    private func sortMessages() {
        messages.sort(by: {$0.dateCreate < $1.dateCreate})
        self.tableView.reloadData()
        self.scrollToBottom()
    }
    
    private func checkAndCreateConversation() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        FBStorageManager.shared.createConversation(customerID: customerID) { (idConversation, error)  in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let id = idConversation {
                self.conversationID = id
                self.observeMessages()
            }
        }
    }
    
    private func scrollToBottom() {
        if messages.count == 0 {
            return
        }
        tableView.scrollToRow(at: IndexPath(row: messages.count - 1, section: 0) , at: .bottom, animated: false)
    }
    
    private func openCamera() {
        let imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }

    private func openGallary() {
        let imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    private func saveImageMessage(with url: URL) {
        guard let user = Auth.auth().currentUser, let conversationID = conversationID else {
            return
        }
        let msg = Message(createBy: user.uid, imageURL: url.absoluteString)
        FBStorageManager.shared.sendMessage(msg, conversationID: conversationID) { [weak self] (error) in
            guard let self = self else { return }
            if let error = error {
                
            }
        }
    }
    
    //MARK: - Actions
    
    @IBAction func didClickSendButton(_ sender: Any) {
        guard let user = Auth.auth().currentUser, let conversationID = conversationID, textView.text != "" else {
            return
        }
        let msg = Message(content: textView.text, createBy: user.uid)
        textView.text = ""
        FBStorageManager.shared.sendMessage(msg, conversationID: conversationID) { [weak self] (error) in
            guard let self = self else { return }
            if let error = error {
                
            }
        }
    }
    
    @IBAction func didClickSelectImage(_ sender: Any) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                self.openCamera()
            }))

            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                self.openGallary()
            }))

            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

//MARK: - TableView
extension ChatVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = messages[indexPath.row]
        if message.type == 0 {
            if message.createBy == Auth.auth().currentUser?.uid {
                let cell = tableView.dequeueReusableCell(withIdentifier: MyTextMessageCell.idCell) as! MyTextMessageCell
                cell.messageLabel.text = message.content
                cell.timeLabel.text = Date.dateString(with: message.dateCreate)
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: TextMessageCell.idCell) as! TextMessageCell
                cell.messageLabel.text = message.content
                cell.timeLabel.text = Date.dateString(with: message.dateCreate)
                return cell
            }
        } else {
            if message.createBy == Auth.auth().currentUser?.uid {
                let cell = tableView.dequeueReusableCell(withIdentifier: MyImageMessageCell.idCell) as! MyImageMessageCell
                cell.msgImageView.sd_setImage(with: URL(string: message.imageURL ?? ""), completed: nil)
                cell.timeLabel.text = Date.dateString(with: message.dateCreate)
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: ImageMessageCell.idCell) as! ImageMessageCell
                cell.msgImageView.sd_setImage(with: URL(string: message.imageURL ?? ""), completed: nil)
                cell.timeLabel.text = Date.dateString(with: message.dateCreate)
                return cell
            }
        }
    }
}

class TextMessageCell: UITableViewCell {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var timeLabel: UILabel!

    static let idCell = "TextMessageCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        messageView.layer.cornerRadius = 12
    }
}

class ImageMessageCell: UITableViewCell {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var msgImageView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!

    static let idCell = "ImageMessageCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        msgImageView.layer.cornerRadius = 12
    }
}

class MyTextMessageCell: UITableViewCell {
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var timeLabel: UILabel!

    static let idCell = "MyTextMessageCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        messageView.layer.cornerRadius = 12
    }
}

class MyImageMessageCell: UITableViewCell {
    
    @IBOutlet weak var msgImageView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!

    static let idCell = "MyImageMessageCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        msgImageView.layer.cornerRadius = 12
    }
}

//MARK: - UIImagePickerControllerDelegate
extension ChatVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let imageURL = info[UIImagePickerController.InfoKey.imageURL] as? URL else {
            return
        }
                
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            FBStorageManager.shared.updateImage(image: pickedImage, name: imageURL.lastPathComponent) { [weak self] (url) in
                guard let self = self else { return }
                if let url = url {
                    self.saveImageMessage(with: url)
                }
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

extension Date {
    var millisecondsSince1970: Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }

    init(seconds: Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(seconds))
    }
    
    static func dateString(with seconds: Int) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(seconds))
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm dd-MM-yyyy"
        return formatter.string(from: date)
    }
}

//MARK: - TextViewDelegate
extension ChatVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == placeholder {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = placeholder
        }
    }
}
