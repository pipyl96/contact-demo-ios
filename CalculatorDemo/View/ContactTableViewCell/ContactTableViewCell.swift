//
//  ContactTableViewCell.swift
//  CalculatorDemo
//
//  Created by PiPyL on 9/26/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit
import SDWebImage

class ContactTableViewCell: UITableViewCell {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var shadowView: UIView!
    
    var indexPath: IndexPath!
    
    static let cellIdentifier: String = "ContactTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()

        avatarImageView.layer.cornerRadius = 20
        avatarImageView.clipsToBounds = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        avatarImageView.image = nil
    }
    
    func setupCell(contactModel: ContactModel, indexPath: IndexPath) {
        self.indexPath = indexPath
        nameLabel.text = contactModel.name
        phoneLabel.text = contactModel.phone
        
        if let avatar = contactModel.avatar, avatar != "", let url = URL(string: avatar) {
            avatarImageView.sd_setImage(with: url, completed: nil)
        }
    }
    
    @IBAction func didTapCallButton(_ sender: Any) {
        
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "ContactTableViewCell", bundle: nil)
    }
}
