//
//  APIController.swift
//  CalculatorDemo
//
//  Created by Lê Phước on 11/10/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit

class APIController: NSObject {
    
    private let baseURL = "https://5fb334a7b6601200168f7211.mockapi.io/api/v1/contacts" //"https://api.jsonbin.io/b/5fadf0ec43fc1e2e1b417d5c/3"//"https://loicontacts.herokuapp.com/contacts"
    
    static let shared = APIController() //Singleton
    
    func getAvatar(urlString: String, completion: @escaping (UIImage?) -> ()) {
        //ktra anh da dc luu duoi local hay chua. neu co thi completion image, neu ko thi tiep tuc xu ly
        guard let url = URL(string: urlString) else {
            completion(nil)
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                completion(nil)
            }
            guard let data = data, let image = UIImage(data: data) else {
                completion(nil)
                return
            }
            //ham luu anh xuong local
            completion(image)
        }
        task.resume()
    }
    
    //GET
    func fetchContacts(completion: @escaping ([ContactModel]?, Error?) -> ()) {
        if let url = URL(string: baseURL) {
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if let error = error {
                    completion(nil, error)
                    return
                }
                if let data = data, let list = try? JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] {
                    var contactList = [ContactModel]()
                    list.forEach { (dict) in
                        let contact = ContactModel(dictionary: dict)
                        contactList.append(contact)
                    }
                    completion(contactList, nil)
                }
            }
            task.resume()
        } else {
            completion(nil, createError(message: "Lỗi URL!"))
        }
    }
    
    //DELETE
    func deleteContact(idContact: String, completion: @escaping (Error?) -> ()) {
        let urlString = baseURL + "/" + idContact
        guard let url = URL(string: urlString ) else {
            completion(createError(message: "Lỗi URL!"))
            return
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "DELETE"
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if let error = error {
                completion(error)
                return
            }
            if let response = response as? HTTPURLResponse, response.statusCode == 200  {
                completion(nil)
                return
            }
            completion(self.createError(message: "Lỗi khi xóa danh bạ!"))
        }
        task.resume()
    }
    
    //POST
    func addContact(with contact: ContactModel, completion: @escaping (ContactModel?, Error?) -> ()) {
        guard let url = URL(string: baseURL) else {
            completion(nil, createError(message: "Lỗi URL!"))
            return
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        guard let data = try? JSONSerialization.data(withJSONObject: contact.toDictionary(), options: []),
              let jsonString = String(data: data, encoding: .utf8),
              let dataRequest = jsonString.data(using: .utf8) else {
            completion(nil, createError(message: "Thêm danh bạ lỗi!"))
            return
        }
        
        urlRequest.httpBody = dataRequest
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if let error = error {
                completion(nil, error)
                return
            }
            if let data = data {
                if let result = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    //tao 1 new contact
                    let contact = ContactModel(dictionary: result)
                    completion(contact, nil)
                    return
                }
            }
            completion(nil, self.createError(message: "Thêm danh bạ lỗi!"))
        }
        
        task.resume()
    }
    
    
    
    
    
    
    
    
    
    
    
    func deleteContacts(idContact: String, completion: @escaping(Error?) -> ()) {
        guard let url = URL(string: baseURL + "/" + idContact) else {
            completion(createError(message: "URL lỗi!"))
            return
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "DELETE"
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if error != nil {
                completion(self.createError(message: "Lỗi khi xóa danh bạ!"))
            }
            
            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                completion(self.createError(message: "Lỗi khi xóa danh bạ!"))
                return
            }
            
            completion(nil)
        }
        
        task.resume()
    }
    
    //MARK: - Helper
    
    private func createError(message: String? = nil) -> NSError {
        return NSError(domain: "com.contacts", code: 400, userInfo: [NSLocalizedDescriptionKey: message ?? "Đã có lỗi xảy ra!"])
    }
}
