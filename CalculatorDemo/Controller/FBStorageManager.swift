//
//  FBStorageManager.swift
//  CalculatorDemo
//
//  Created by flydino on 12/15/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit
import FirebaseStorage
import FirebaseDatabase
import Firebase

class FBStorageManager: NSObject {
    
    static let shared = FBStorageManager()
    
    private override init() {
        super.init()
    }
    
    let storageRef = Storage.storage().reference()
    let databaseRef = Database.database().reference()
    
    func getAllUsers(completion: @escaping(([ContactModel]?, Error?) -> Void)) {
        databaseRef.child("Users").observeSingleEvent(of: .value) { (snapshot) in
            var contacts = [ContactModel]()
            if let dict = snapshot.value as? [String: Any] {
                dict.keys.forEach { (key) in
                    if let contact = dict[key] as? [String: Any] {
                        let contact = ContactModel(dictionary: contact)
                        contacts.append(contact)
                    }
                }
                completion(contacts, nil)
            }
        } withCancel: { (error) in
            completion(nil, error)
        }
    }
    
    func registerAccount(id: String, mail: String, completion: @escaping ((Error?) -> ())) {
        let data = [
            "uid": id,
            "email": mail
        ]
        databaseRef.child("Users/\(id)").setValue(data) { (error, dataRef) in
            if let error = error {
                completion(error)
                return
            }
            completion(nil)
        }
    }
    
    //MARK: - Chat
    func sendMessage(_ message: Message, conversationID: String, completion: @escaping ((Error?) -> ())) {
        databaseRef.child("Conversations/\(conversationID)/contents/\(message.dateCreate)").setValue(message.toDictionary()) { (error, dataRef) in
            if let error = error {
                completion(error)
                return
            }
            completion(nil)
        }
    }
    
    func getMessages(conversationID: String, completion: @escaping (([Message]?, Error?) -> ())) {
        databaseRef.child("Conversations/\(conversationID)/contents").observe(.value) { (snapshot) in
            if let dict = snapshot.value as? [String: Any] {
                var messages = [Message]()
                dict.keys.forEach { (key) in
                    if let messageDict = dict[key] as? [String: Any] {
                        let message = Message(dictionary: messageDict)
                        messages.append(message)
                    }
                }
                completion(messages, nil)
            } else {
                completion(nil, self.createError(message: "Loi khi get messages"))
            }
        } withCancel: { (error) in
            completion(nil, error)
        }

    }
    
    func createConversation(customerID: String, completion: @escaping ((String?, Error?) -> ())) {
        
        //Kiem tra conversation cua current user da co hay chua
        //Neu chua co thi tao conversation moi o table Conversations va add id conversation den conversations cua current user. Neu da co conversation thi completion
        //Neu conversations cua current user chua co conversation nao thi setvalue neu da ton tai thi append vao. Tuong tu doi voi customer
        //Kiem tra conversation cua customer da co hay chua
        
        guard let user = Auth.auth().currentUser else {
            return
        }
        var idConversation: String? = "\(user.uid)\(customerID)"
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        databaseRef.child("Users/\(user.uid)/conversations").observeSingleEvent(of: .value) { (snapshot) in
            dispatchGroup.leave()
            if var snapshot = snapshot.value as? [String] {
                let isExist = snapshot.filter { //Kiem tra user da ton tai conversation voi custumer chau
                    return $0 == "\(user.uid)\(customerID)" || $0 == "\(customerID)\(user.uid)"
                }
                if isExist.count == 0 { //Neu co thi append id conversation moi vao
                    snapshot.append("\(user.uid)\(customerID)")
                    dispatchGroup.enter()
                    self.databaseRef.child("Users/\(user.uid)/conversations").setValue(snapshot) { (error, dataRef) in
                        if let error = error {
                            completion(nil, error)
                            return
                        }
                        dispatchGroup.leave()
                    }
                    dispatchGroup.enter()
                    self.databaseRef.child("Conversations/\(user.uid)\(customerID)/users").setValue([user.uid, customerID]) { (error, dataRef) in
                        if let error = error {
                            completion(nil, error)
                            return
                        }
                        dispatchGroup.leave()
                    }
                } else {
                    idConversation = isExist.first
                }
            } else {
                dispatchGroup.enter()
                self.databaseRef.child("Users/\(user.uid)/conversations").setValue(["0": "\(user.uid)\(customerID)"]) { (error, dataRef) in
                    if let error = error {
                        completion(nil, error)
                        return
                    }
                    dispatchGroup.leave()
                }
                dispatchGroup.enter()
                self.databaseRef.child("Conversations/\(user.uid)\(customerID)/users").setValue([user.uid, customerID]) { (error, dataRef) in
                    if let error = error {
                        completion(nil, error)
                        return
                    }
                    dispatchGroup.leave()
                }
            }
        }
        
        dispatchGroup.enter()
        databaseRef.child("Users/\(customerID)/conversations").observeSingleEvent(of: .value) { (snapshot) in
            dispatchGroup.leave()
            if var snapshot = snapshot.value as? [String] {
                let isExist = snapshot.filter {
                    return $0 == "\(user.uid)\(customerID)" || $0 == "\(customerID)\(user.uid)"
                }
                if isExist.count == 0 {
                    snapshot.append("\(user.uid)\(customerID)")
                    dispatchGroup.enter()
                    self.databaseRef.child("Users/\(customerID)/conversations").setValue(snapshot) { (error, dataRef) in
                        if let error = error {
                            completion(nil, error)
                            return
                        }
                        dispatchGroup.leave()
                    }
                }
            } else {
                dispatchGroup.enter()
                self.databaseRef.child("Users/\(customerID)/conversations").setValue(["0": "\(user.uid)\(customerID)"]) { (error, dataRef) in
                    if let error = error {
                        completion(nil, error)
                        return
                    }
                    dispatchGroup.leave()
                }
            }
            dispatchGroup.notify(queue: .main) {
                completion(idConversation, nil)
            }
        }
    }
    
    //MARK: - Update Info
    func updateInfo(_ info: [String: Any], completion: @escaping((Error?) -> ())) {
        guard let user = Auth.auth().currentUser else {
            return
        }
        databaseRef.child("Users/\(user.uid)").updateChildValues(info) { (error, dataRef) in
            if let error = error {
                completion(error)
                return
            }
            completion(nil)
        }
    }
    
    func updateImage(image: UIImage, name: String, completion: @escaping ((URL?) -> ())) {
        guard let data = image.jpegData(compressionQuality: 0.3) else {
            return
        }
        let ref = storageRef.child("Avatar/\(name)")
        ref.putData(data, metadata: nil) { (metadata, error) in
            guard metadata != nil else {
                completion(nil)
                return
            }
            ref.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    completion(nil)
                    return
                }
                completion(downloadURL)
                return
            }
        }
    }
    
    //MARK: - Helper
    
    private func createError(message: String? = nil) -> NSError {
        return NSError(domain: "com.contacts", code: 400, userInfo: [NSLocalizedDescriptionKey: message ?? "Đã có lỗi xảy ra!"])
    }
}
