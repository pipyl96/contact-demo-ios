//
//  FakeDataManager.swift
//  CalculatorDemo
//
//  Created by flydino on 12/21/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit

class FakeDataManager: NSObject {
    static let shared = FakeDataManager()
    
    private override init() {
        super.init()
    }
    
    var data = [
        Message(content: "Hello world", createBy: "user1"),
        Message(content: "Meta mới try hard là hơi khó .... \nĐến giờ cờ quạt ae ơi ", createBy: "user1"),
        Message(content: "Hello world", createBy: "user1"),
        Message(createBy: "user1", imageURL: "https://s3.cloud.cmctelecom.vn/xemgame-com/2020/05/08/MC-Minh-Nghi-cong-khai-nguoi-yeu-2.jpg"),
        Message(content: "Mách nhỏ với các bạn, mùa noel năm nay không mặc áo đẹp cùng hội bạn thân đến KDL Ba Hồ - Ninh Hòa là một thiếu sót đó nghe ", createBy: "user1"),
        Message(createBy: "user1", imageURL: "https://image.thanhnien.vn/800/uploaded/thuyhang/2017_10_29/img_0255_lpou.jpg"),
        Message(content: "Hi", createBy: "user1"),
        Message(content: "Hello world", createBy: "user1"),
        Message(content: "Good morning! How are you, today?", createBy: "user1"),
        Message(createBy: "user1", imageURL: "https://www.brandsvietnam.com/upload/news/480px/2019/18222_SteveJobs.jpg")
    ]
}
