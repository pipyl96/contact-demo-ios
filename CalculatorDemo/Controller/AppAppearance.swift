//
//  AppAppearance.swift
//  CalculatorDemo
//
//  Created by flydino on 11/17/20.
//  Copyright © 2020 PiPyL. All rights reserved.
//

import UIKit

class AppAppearance: NSObject {
    static func config() {
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 19, weight: .medium)]
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().barTintColor = UIColor.systemBlue
        
        UINavigationBar.appearance().backIndicatorImage = UIImage(named: "ic_back")
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = UIImage(named: "ic_back")
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -1000, vertical: 0), for: .default)
        
//        UITabBar.appearance().barTintColor = .defaultColor
//        UITabBar.appearance().tintColor = .white
//        UITabBar.appearance().unselectedItemTintColor = .white
    }

}
